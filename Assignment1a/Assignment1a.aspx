﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Assignment1a.aspx.cs" Inherits="Assignment1a.Assignment1a" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Car Sharing Service | Moonlight </title>
    <link href="Style.css" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server">
       
            <h1>MOONLIGHT - test</h1> 
            <p>CAR SHARING SERVICE</p>
            <br />
             <asp:validationsummary forecolor="Red" runat="server" id="validationSummary">
                </asp:validationsummary>
            <h3>PLACE YOUR ORDER</h3>
        
           
            <div class="fname">
                   <asp:Label Text="First Name:" runat="server" />
                <asp:TextBox runat="server" ID="firstName" placeholder="First Name"></asp:TextBox>
                <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter your First Name" ControlToValidate="firstName" ID="validatorfName"></asp:RequiredFieldValidator>
            </div>
           
            <div class="lname">
                    <asp:Label Text="Last Name:" runat="server" />
                <asp:TextBox runat="server" ID="lastName" placeholder="Last Name"></asp:TextBox>
                <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter your Last Name" ControlToValidate="lastName" ID="validatorlName"></asp:RequiredFieldValidator>
            </div>
            
            <div>
                   <asp:Label Text="Email:" runat="server" />
                <asp:TextBox runat="server" ID="clientEmail" placeholder="Email"></asp:TextBox>
                <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter an Email" ControlToValidate="clientEmail" ID="RequiredFieldValidator1"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="regexEmailValid" runat="server" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ControlToValidate="clientEmail" ErrorMessage="Invalid Email Format"></asp:RegularExpressionValidator>
            </div>
            
            <div>
                   <asp:Label Text="Date of Birth:" runat="server" />
                <asp:TextBox runat="server" ID="birthDate" placeholder="DD/MM/YY"></asp:TextBox>
                <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter your Date of Birth" ControlToValidate="birthDate" ID="validatorbDate"></asp:RequiredFieldValidator>
            </div>
            
            <div>
                   <asp:Label Text="Address:" runat="server" />
                <asp:TextBox runat="server" ID="clientAddress" placeholder="e.g. 22 Prince Edward Dr "></asp:TextBox>
                <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter your Address" ControlToValidate="clientAddress" ></asp:RequiredFieldValidator>
            </div>
            
            <div>
                   <asp:Label Text="Postal Code:" runat="server" />
                <asp:TextBox runat="server" ID="clientPostal" placeholder="e.g. A1A1A1"></asp:TextBox>
                <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter your Postal Code" ControlToValidate="clientPostal" ></asp:RequiredFieldValidator>
            </div>
            
            <div>
                   <asp:Label Text="Phone Number:" runat="server" />
                <asp:TextBox runat="server" ID="clientPhone" placeholder="Phone"></asp:TextBox>
                <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter a Phone Number" ControlToValidate="clientPhone" ID="validatorPhone"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator2" 
                        runat="server" ErrorMessage="This is an invalid phone number" 
                        ControlToValidate ="clientPhone" 
                        ValidationExpression= "^([0-9\(\)\/\+ \-]*)$"></asp:RegularExpressionValidator>
           
            </div>
            
            <div>
                 <asp:Label Text="Vehicles:" runat="server" />
               <asp:DropDownList runat="server" ID="carModel">
                    <asp:ListItem Value="Select" Text="-Select-"></asp:ListItem>
                    <asp:ListItem Value="Crossover" Text="Crossover | SUV"></asp:ListItem>
                    <asp:ListItem Value="Compact" Text="Compact | Sedan"></asp:ListItem>
                    <asp:ListItem Value="Hybrid" Text="Hybrid | Electric"></asp:ListItem>
               </asp:DropDownList>
            </div>
                 <br />
          
           <fieldset >
             
            <asp:RadioButton runat="server" Text="Delivery" GroupName="via" checked="true"/>
            <asp:RadioButton runat="server" Text="Pick-Up" GroupName="via"/>
               <br />
            <asp:Label Text="Pick-up or Delivery Time:" runat="server" />
            <asp:TextBox runat="server" ID="pickupTime" placeholder="24 hour format e.g 1000"></asp:TextBox>
            <asp:RangeValidator runat="server" Type="Integer" 
            MinimumValue ="1000" MaximumValue="1900" ControlToValidate="pickupTime" 
            ErrorMessage="Our hours are between 10am and 7pm" />
              <br />
          </fieldset>
           
          <div >
                    <asp:Label Text="Features:" runat="server" />
                    <br />
                <asp:CheckBox runat="server" ID="carFeature1" Text="Heated Seats" />
                <asp:CheckBox runat="server" ID="carFeature2" Text="Heated Steering Wheel" />
                <asp:CheckBox runat="server" ID="carFeature3" Text="Adaptive Cruise Control (ACC)" />
                <asp:CheckBox runat="server" ID="carFeature4" Text="Panoramic Sunroof" />
                <asp:CheckBox runat="server" ID="carFeature5" Text="Blind-spot Detector" />
          </div>
            
        <div class="button">
                <asp:Button Text="Submit" runat="server" />
        </div>
    </form>
</body>
</html>
